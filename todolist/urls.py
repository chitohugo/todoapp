from django.urls import path, reverse_lazy
from django.contrib.auth.views import LoginView, LogoutView
from todolist.views import TodoList, TodoCreate, TodoUpdate, TodoDelete, Mark_Homework, SignUp
from todolist.templates.forms.form_login import LoginForm
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', LoginView.as_view(authentication_form=LoginForm), name='login'),
    path('login/', LoginView.as_view(authentication_form=LoginForm), name='login'),
    path('logout', LogoutView.as_view(next_page=reverse_lazy('login')), name='logout'),
    path('signup/', SignUp.as_view(), name='signup'),
    path('list/', login_required(TodoList.as_view()), name='todo-list'),
    path('create/', login_required(TodoCreate.as_view()), name='todo-create'),
    path('update/<int:pk>/', login_required(TodoUpdate.as_view()), name='todo-update'),
    path('delete/<int:pk>/', login_required(TodoDelete.as_view()), name='todo-delete'),
    path('mark/<int:pk>', login_required(Mark_Homework.as_view()), name='mark-homework'),
]
