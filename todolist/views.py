from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, View
from django.contrib.auth.forms import UserCreationForm

from todolist.models import Todo


class TodoList(ListView):
    model = Todo


class TodoCreate(CreateView):
    success_url = reverse_lazy('todo-list')
    model = Todo
    fields = '__all__'

    def form_valid(self, form):
        model = form.save(commit=False)
        model.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)


class TodoUpdate(UpdateView):
    model = Todo
    fields = ['item', 'description', 'completed']
    success_url = reverse_lazy('todo-list')


class TodoDelete(DeleteView):
    success_url = reverse_lazy('todo-list')
    model = Todo

    def get_success_url(self):
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)


class SignUp(CreateView):
    form_class = UserCreationForm
    success_url = success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


class Mark_Homework(View):
    success_url = reverse_lazy('todo-list')

    def get(self, request, pk):
        item = Todo.objects.get(pk=pk)
        item.completed = True
        item.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)
